package springapp.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import springapp.domain.Student;

@Repository("studentDao")
public class JdbcStudentDao implements StudentDao {

	/** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
    	this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public JdbcTemplate getJdbcTemplate(){
    	return this.jdbcTemplate;
    }
    
	/**
	 * @author dhamo
	 *
	 */
	private static class StudentMapper implements
			ParameterizedRowMapper<Student> {

		/* (non-Javadoc)
		 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
		 */
		public Student mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			Student student = new Student();
			student.setRollNumber(resultSet.getInt("ROLLNUM"));
			student.setName(resultSet.getString("NAME"));
			student.setAge(resultSet.getInt("AGE"));
			student.setSex(resultSet.getString("SEX").charAt(0));
			student.setGrade(resultSet.getString("GRADE"));
			student.setCountry(resultSet.getString("COUNTRY"));
			return student;
		}

	}
		
    public List<Student> getStudentList() {
		logger.info("Getting Students");

		List<Student> studentList = getJdbcTemplate().query(
				"SELECT ROLLNUM, NAME, AGE, SEX,GRADE,COUNTRY FROM STUDENT_INFO WHERE IS_ACTIVE='Y'",
				new StudentMapper());
		logger.info("Student List"+studentList);
		return studentList;
	}

	public void saveStudent(Student student) {
		
		logger.info("Saving Student:"+student.getName());

		Object updObj[] = new Object[]{student.getRollNumber(), student.getName(), student.getAge(), student.getSex(), student.getGrade(), student.getCountry()};
		int updArgTypes[] = new int[]{Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		String updStmt = "INSERT INTO STUDENT_INFO(ROLLNUM, NAME, AGE, SEX, GRADE, COUNTRY) VALUES(?, ?, ?, ?, ?, ?)";
		
		int updCount = this.getJdbcTemplate().update(updStmt, updObj, updArgTypes);
		logger.info("Rows affected :"+updCount);
	}

	public Student getStudent(int rollNumber) {
		List<Student> studentList = getJdbcTemplate().query(
				"SELECT ROLLNUM, NAME, AGE, SEX,GRADE,COUNTRY FROM STUDENT_INFO WHERE ROLLNUM="+rollNumber,
				new StudentMapper());
		logger.info("Student List"+studentList);
		return studentList.get(0);
	}

	public void updateStudent(Student student) {
		logger.info("Updating Student:"+student.getName());

		Object updObj[] = new Object[]{
				student.getAge(), 
				student.getSex(), 
				student.getGrade(), 
				student.getCountry(),
				student.getRollNumber() };
		int updArgTypes[] = new int[]{Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER};
		String updStmt = "UPDATE STUDENT_INFO " +
										" SET AGE=?,"+
										" SEX=?, "+
										" GRADE=?, "+
										" COUNTRY =? "+
										" WHERE ROLLNUM=? ";
		
		
		int updCount = this.getJdbcTemplate().update(updStmt, updObj, updArgTypes);
		logger.info("Update Student - Rows affected :"+updCount);
		
	}

	public void deleteStudent(int rollNumber) {
		logger.info("Deleting Student:"+ rollNumber);

		Object updObj[] = new Object[]{ rollNumber};
		int updArgTypes[] = new int[]{Types.INTEGER};
		String updStmt = "UPDATE STUDENT_INFO " +
						" SET IS_ACTIVE='N' "+
						" WHERE ROLLNUM=? ";
		
		
		int updCount = this.getJdbcTemplate().update(updStmt, updObj, updArgTypes);
		logger.info("Update Student - Rows affected :"+updCount);
		
	}
}