package hibernate.hibernate_one_to_many;

import java.sql.Date;
import java.util.Calendar;

import org.hibernate.Session;

import damapp.domain.Stock;
import damapp.domain.StockDailyRecord;
import damapp.domain.StockDetail;
import damapp.persistence.HibernateUtil;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
		System.out.println("Hibernate one to one (XML mapping)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7700");
        stock.setStockName("Keyboard");
//        session.save(stock);
        
		StockDetail stockDetail = new StockDetail();
		stockDetail.setCompName("Supraja Store India");
		stockDetail.setCompDesc("Best resort in the world");
		stockDetail.setRemark("Nothing Special");
		stockDetail.setListedDate(new Date(System.currentTimeMillis()));

		stock.setStockDetail(stockDetail);
		stockDetail.setStock(stock);

//		session.save(stock);

        StockDailyRecord stockDailyRecords = new StockDailyRecord();
        stockDailyRecords.setPriceOpen(new Float("1.2"));
        stockDailyRecords.setPriceClose(new Float("1.1"));
        stockDailyRecords.setPriceChange(new Float("10.0"));
        stockDailyRecords.setVolume(3000000L);
        stockDailyRecords.setDate(new Date(System.currentTimeMillis()));
        
        stockDailyRecords.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords);

        StockDailyRecord stockDailyRecords2 = new StockDailyRecord();
        stockDailyRecords2.setPriceOpen(new Float("2.2"));
        stockDailyRecords2.setPriceClose(new Float("2.1"));
        stockDailyRecords2.setPriceChange(new Float("20.0"));
        stockDailyRecords2.setVolume(6000000L);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        stockDailyRecords2.setDate(cal.getTime());
        
        stockDailyRecords2.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords2);
		session.save(stock);
        
        session.save(stockDailyRecords);
        session.save(stockDailyRecords2);
        session.getTransaction().commit();

		System.out.println("Done");
        
        HibernateUtil.shutdown();
     }
}
